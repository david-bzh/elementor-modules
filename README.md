# ![logo](elementor/assets/images/logo-elementor.png) Elementor-Modules

## Description

Add new modules to elementor.

-   Version: 1.0.0
-   Requires PHP: 7.1

## Installation

1. Move folder elementor-modules in folder wp-content > plugins
2. In your site's admin panel, go to Plugins > Installed Plugins.
3. Search "Elementor Modules" in the list of plugins.
4. Click Activate to start using le plugin.
