<?php
/**
 * Plugin Name: Elementor Modules
 * Description: Add new modules to elementor.
 * Plugin URI: https://gitlab.com/David-_-/elementor-modules
 * Author: David-_-
 * Version: 1.0.0.
 * Requires PHP 7.1
 *
 * @package Elementor_Modules
 *
 * Text Domain: elementor-modules
 */

declare( strict_types = 1 );

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

if ( is_readable( __DIR__ . '/elementor/class-setup-manager.php' ) ) {

	/**
	* Fires once activated plugins have loaded.
	*
	* @source https://developer.wordpress.org/reference/hooks/plugins_loaded/
	*
	* Pluggable functions are also available at this point in the loading order.
	* @return void
	*/
	add_action( 'plugins_loaded', 'elementor_modules_setup', 11 );

	/**
	 * Elementor modules setup
	 *
	 * Setup plugins Elementor_Modules
	 *
	 * @return void
	 */
	function elementor_modules_setup() : void {

		if ( class_exists( 'Elementor\Plugin' ) ) {
			load_plugin_textdomain( 'elementor-modules' );

			require 'elementor/class-setup-manager.php';
		} else {
			add_action( 'admin_notices', 'admin_notice_error' );
		}
	}

	/**
	 * Admin notice error
	 *
	 * Add in admin notice error
	 *
	 * @since 1.0.0
	 * @return void
	 */
	function admin_notice_error() : void {

		$plugin = 'elementor/elementor.php';

		if ( is_elementor_installed() ) {
			if ( ! current_user_can( 'activate_plugins' ) ) {
				return;
			}

			$message      = __( 'Elementor Modules is not working because you need to activate the Elementor plugin.', 'elementor-modules' );
			$button_text  = __( 'Activate Elementor Now', 'elementor-modules' );
			$button_url   = wp_nonce_url( 'plugins.php?action=activate&amp;plugin=' . $plugin . '&amp;plugin_status=all&amp;paged=1&amp;s', 'activate-plugin_' . $plugin );

		} else {
			if ( ! current_user_can( 'install_plugins' ) ) {
				return;
			}

			$message     = __( 'Elementor Modules is not working because you need to install the Elementor plugin.', 'elementor-modules' );
			$button_text = __( 'Install Elementor Now', 'elementor-modules' );
			$button_url  = wp_nonce_url( self_admin_url( 'update.php?action=install-plugin&plugin=elementor' ), 'install-plugin_elementor' );
		}

		printf( '<div class="error"><p><p>%1$s</p><p><a href="%2$s" class="button-primary">%3$s</a></p></p></div>', esc_html( $message ), esc_url( $button_url ), esc_html( $button_text ) );

	}

	if ( ! function_exists( 'is_elementor_installed' ) ) {

		/**
		 * Is elementor installed
		 *
		 * Check is elementor installed
		 *
		 * @since 1.0.0
		 * @return bool
		 */
		function is_elementor_installed() : bool {
			$file_path = 'elementor/elementor.php';
			$installed_plugins = get_plugins();

			return isset( $installed_plugins[ $file_path ] );
		}
	}
}
