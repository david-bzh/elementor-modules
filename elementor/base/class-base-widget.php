<?php
/**
 * Base_Widget class
 *
 * @package Elementor_Modules
 * @since 1.0.0
 */

declare( strict_types = 1 );

namespace ElementorModules\Base;

use Elementor\Widget_Base;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

/**
 * ElementorModules base widget.
 *
 * An abstract class to register new Elementor widgets.
 * It extended the `Widget_Base` class to inherit its properties.
 *
 * @version 1.0.0
 * @abstract
 */
abstract class Base_Widget extends Widget_Base {

	/**
	 * Get directory widget
	 *
	 * Get the widget directory.
	 *
	 * @since 1.0.0
	 * @access private
	 *
	 * @return string
	 */
	private function get_directory_widget(): string {
		return dirname( __DIR__ ) . '/modules/' . $this->get_name();
	}

	/**
	 * Get widget categories.
	 *
	 * Retrieve the widget categories.
	 *
	 * @source https://code.elementor.com/methods/elementor-widget_base-get_categories/
	 * @since 1.0.0
	 * @access public
	 *
	 * @return array Widget categories.
	 */
	public function get_categories(): array {
		return array( 'latelier' );
	}

	/**
	 * Register widget controls.
	 *
	 * Adds different input fields to allow the user to change and customize the widget settings.
	 *
	 * @since 1.0.0
	 * @access protected
	 *
	 * @return void
	 */
	protected function _register_controls(): void { //phpcs:ignore

		require apply_filters( 'elementor_modules_register_controls_' . $this->get_name(), $this->get_directory_widget() . '/controls/register-controls.php' );
	}

	/**
	 * Render widget output on the frontend.
	 *
	 * Written in PHP and used to generate the final HTML.
	 *
	 * @since 1.0.0
	 * @access protected
	 *
	 * @return void
	 */
	protected function render(): void {

		require apply_filters( 'elementor_modules_render_' . $this->get_name(), $this->get_directory_widget() . '/widgets/templates/render.php' );
	}

}
