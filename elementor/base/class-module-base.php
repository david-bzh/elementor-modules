<?php
/**
 * Module_Base class
 *
 * @package Elementor_Modules
 * @since 1.0.0
 */

declare( strict_types = 1 );

namespace ElementorModules\Base;

use Elementor\Core\Base\Module;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

/**
 * ElementorModules module base.
 *
 * An abstract class to register new Elementor widgets.
 * It extended the `Module` class to inherit its properties.
 *
 * @version 1.0.0
 * @abstract
 */
abstract class Module_Base extends Module {

	/**
	 * Registered widgets.
	 *
	 * Holds the list of all the registered widgets.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @var Module_Base
	 *
	 * @return array
	 */
	public function get_widgets(): array {
		return array();
	}

	/**
	 * Modules base constructor.
	 *
	 * Initializing the class `Widgets_Manager` Elementor.
	 *
	 * @since 1.0.0
	 * @access public
	 */
	public function __construct() {

		/**
		 * After widgets registered.
		*
		* @since 1.0.0
		* @source https://code.elementor.com/hooks/elementor-widgets-widgets_registered-4/
		*
		* Fires after Elementor widgets are registered.
		*/
		add_action( 'elementor/widgets/widgets_registered', array( $this, 'init_widgets' ), 11 );
	}

	/**
	 * Init widgets.
	 *
	 * Initialize Elementor widgets manager. Include all the the widgets files
	 * and register each Elementor and WordPress widget.
	 *
	 * @since 1.0.0
	 * @access public
	 * @codingStandardsIgnoreStart
	 * @template t of object<Elementor\Widgets_Manager>
	 * @param t $widgets_manager
	 * @return void
	 */
	public function init_widgets( object $widgets_manager ):void {
		// @codingStandardsIgnoreEnd
		foreach ( $this->get_widgets() as $widget ) {
			$class_name = $this->get_reflection()->getNamespaceName() . '\Widgets\\' . $widget;

			$widgets_manager->register_widget_type( new $class_name() );
		}
	}
}
