<?php
/**
 * Setup_Manager class
 *
 * @package Elementor_Modules
 * @since 1.0.0
 */

declare( strict_types = 1 );

namespace ElementorModules;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

use ElementorModules\Core\Elements_Manager;
use ElementorModules\Core\Modules_Manager;

/**
 * Setup Manager.
 *
 * The main ElementorModules handler class is responsible for initializing.
 * The class registers and all the components required to run the ElementorModules.
 *
 * @version 1.0.0
 */
class Setup_Manager {

	/**
	 * Instance.
	 *
	 * Holds the plugin instance.
	 *
	 * @since 1.0.0
	 * @access public
	 * @static
	 *
	 * @var Setup_Manager|null
	 */
	public static $instance = null;


	/**
	 * Setup.
	 *
	 * Ensures only one instance of the Setup Manager class is loaded or can be loaded.
	 *
	 * @since 1.0.0
	 * @access public
	 * @static
	 *
	 * @return Setup_Manager An instance of the class.
	 */
	public static function setup() {

		if ( is_null( self::$instance ) ) {
			self::$instance = new self();

		}
		return self::$instance;
	}

	/**
	 * Setup Manager constructor.
	 *
	 * Initializing ElementorModules.
	 *
	 * @since 1.0.0
	 * @access private
	 */
	private function __construct() {

		require_once 'includes/class-autoloader.php';
		Autoloader::run();

		new Elements_Manager();
		new Modules_Manager();
	}

}
Setup_Manager::setup();
