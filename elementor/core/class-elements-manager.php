<?php
/**
 * Elements_Manager class
 *
 * @package Elementor_Modules
 * @since 1.0.0
 */

declare(strict_types = 1);

namespace ElementorModules\Core;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

/**
 * Elementor elements manager.
 *
 * Elementor elements manager handler class is responsible for registering and
 * initializing all the supported elements.
 *
 * @version 1.0.0
 */
class Elements_Manager {


	/**
	 * Elements manager constructor.
	 *
	 * Initializing the class `Elements_Manager` Elementor.
	 *
	 * @since 1.0.0
	 * @access public
	 */
	public function __construct() {

		/**
		 * When categories are registered.
		 *
		 * @since 1.0.0
		 * @source https://code.elementor.com/hooks/elementor-elements-categories_registered/
		 *
		 * Fires after basic categories are registered, before WordPress category have been registered.
		 * This is where categories registered by external developers are added.
		 */
		add_action( 'elementor/elements/categories_registered', array( $this, 'init_categories' ), 11, 1 );
	}

	/**
	 * Init categories.
	 *
	 * Initialize the element categories.
	 *
	 * @since 1.0.0
	 * @access public
	 * @codingStandardsIgnoreStart
	 * @template t of object<Elementor\Widgets_Manager>
	 * @param t $elements_manager
	 * @return void
	 */
	public function init_categories( object $elements_manager ):void {
		// @codingStandardsIgnoreEnd
		$elements_manager->add_category(
			'latelier',
			array(
				'title' => __( 'Latelier', 'elementor-modules' ),
				'icon'  => 'fa fa-plug',
			)
		);
	}
}


