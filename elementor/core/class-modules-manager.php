<?php
/**
 * Modules_Manager class
 *
 * @package Elementor_Modules
 * @since 1.0.0
 */

declare( strict_types = 1 );

namespace ElementorModules\Core;

use ElementorModules\Base\Module_Base;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

/**
 * Elementor Widget modules manager.
 *
 * Elementor Widget modules manager handler class is responsible for registering and
 * managing Elementor modules.
 *
 * @version 1.0.0
 */
final class Modules_Manager {

	/**
	 * Registered modules.
	 *
	 * Holds the list of all the registered modules.
	 *
	 * @since 1.0.0
	 * @access private
	 *
	 * @var Module_Base
	 */
	private $modules = array();

	/**
	 * Modules manager constructor.
	 *
	 * Initializing the class `Module_Base` ElementorModules.
	 *
	 * @since 1.0.0
	 * @access public
	 */
	public function __construct() {

		$modules = apply_filters(
			'elementor_widget_init_modules',
			array(
				'exemple-modules',
			)
		);

		foreach ( $modules as $module_name ) {

			$class_name = str_replace( '-', ' ', $module_name );
			$class_name = str_replace( ' ', '', ucwords( $class_name ) );
			$class_name = '\ElementorModules\Modules\\' . $class_name . '\Module';

			/**
			 * Module_Base class.
			 *
			 * @var Module_Base $class_name
			 */
			if ( $class_name::is_active() ) {
				$this->modules[ $module_name ] = $class_name::instance();
			}
		}
	}

	/**
	 * Get modules.
	 *
	 * Retrieve all the registered modules or a specific module.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @param string $module_name Module name.
	 *
	 * @return null|Module_Base|Module_Base[] All the registered modules or a specific module.
	 */
	public function get_modules( string $module_name ) {
		if ( $module_name ) {
			if ( isset( $this->modules[ $module_name ] ) ) {
				return $this->modules[ $module_name ];
			}

			return null;
		}

		return $this->modules;
	}

}
