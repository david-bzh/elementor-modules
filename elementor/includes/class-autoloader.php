<?php
/**
 * Autoloader class
 *
 * @package Elementor_Modules
 * @since 1.0.0
 */

namespace ElementorModules;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

/**
 * ElementorModules autoloader.
 *
 * ElementorModules autoloader handler class is responsible for loading the different
 * classes needed to run the setup manager.
 *
 * @version 1.0.0
 */
class Autoloader {

	/**
	 * Run autoloader.
	 *
	 * Register a function as `__autoload()` implementation.
	 *
	 * @since 1.0.0
	 * @access public
	 * @static
	 *
	 * @return void
	 */
	public static function run():void {
		spl_autoload_register( array( __CLASS__, 'autoload' ) );
	}

	/**
	 * Classes aliases.
	 *
	 * Maps Elementor classes to aliases.
	 *
	 * @since 1.0.0
	 * @access private
	 * @static
	 *
	 * @return array
	 */
	public static function get_classes_aliases():array {
		return array();
	}

	/**
	 * Autoload.
	 *
	 * For a given class, check if it exist and load it.
	 *
	 * @since 1.0.0
	 * @access private
	 * @static
	 *
	 * @param string $class Class name.
	 * @return void
	 */
	private static function autoload( $class ):void {
		if ( 0 !== strpos( $class, __NAMESPACE__ ) ) {
			return;
		}

		$classes_aliases = self::get_classes_aliases();
		$has_class_alias = isset( $classes_aliases[ $class ] );

		if ( $has_class_alias ) {
			$class_to_load = $classes_aliases[ $class ];
		} else {
			$class_to_load = $class;
		}

		if ( ! class_exists( $class_to_load ) ) {
			$filename = strtolower(
				preg_replace(
					array( '/^' . __NAMESPACE__ . '\\\/', '/([a-z])([A-Z])/', '/_/', '/\\\/', '/\/(?!.*\/.*)/' ),
					array( '', '$1-$2', '-', DIRECTORY_SEPARATOR, '/class-' ),
					$class_to_load
				)
			);

			$filename = dirname( __DIR__ ) . '/' . $filename . '.php';
			if ( is_readable( $filename ) ) {
				include $filename;
			}
		}

		if ( $has_class_alias ) {
			class_alias( $class_to_load, $class );
		}
	}

}
