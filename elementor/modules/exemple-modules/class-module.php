<?php
/**
 * Module class
 *
 * @package Elementor_Modules
 * @since 1.0.0
 */

declare( strict_types = 1 );

namespace ElementorModules\Modules\ExempleModules;

use ElementorModules\Base\Module_Base;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

/**
 * Elementor widget module.
 *
 * Elementor widget module handler class is responsible for registering
 * and managing Elementor widget modules.
 *
 * @version 1.0.0
 */
class Module extends Module_Base {

	/**
	 * Get widget.
	 *
	 * Retrieve the widgets.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return array Widgets.
	 */
	public function get_widgets():array {
		return array(
			'Exemple_Modules',
		);
	}

	/**
	 * Get module name.
	 *
	 * Retrieve the module name.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Module name.
	 */
	public function get_name() {
		return 'exemple-modules';
	}
}
