<?php
/**
 * Controles register controls
 *
 * @var Module_Base $this
 *
 * @package Elementor_Modules
 * @since 1.0.0
 */

$this->start_controls_section(
	'content_section',
	array(
		'label' => __( 'Content', 'elementor-modules' ),
		'tab'   => \Elementor\Controls_Manager::TAB_STYLE,
	)
);

$this->add_control(
	'url',
	array(
		'label'       => __( 'URL to embed', 'elementor-modules' ),
		'type'        => \Elementor\Controls_Manager::TEXT,
		'input_type'  => 'url',
		'placeholder' => __( 'https://your-link.com', 'elementor-modules' ),
	)
);

$this->end_controls_section();
