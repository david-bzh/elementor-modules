<?php
/**
 * Exemple_Modules class
 *
 * @package Elementor_Modules
 * @since 1.0.0
 */

declare( strict_types = 1 );

namespace ElementorModules\Modules\ExempleModules\Widgets;

use ElementorModules\Base\Base_Widget;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

/**
 * Elementor Make Coffee Widget.
 *
 * Elementor widget that inserts an embbedable content into the page, from any given URL.
 *
 * @source https://developers.elementor.com/creating-a-new-widget/ Creating a New Widget
 * @version 1.0.0
 */
class Exemple_Modules extends Base_Widget {

	/**
	 * Get widget name.
	 *
	 * Retrieve make-coffee widget name.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget name.
	 */
	public function get_name():string {
		return 'exemple-modules';
	}

	/**
	 * Get widget title.
	 *
	 * Retrieve make-coffee widget title.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget title.
	 */
	public function get_title():string {
		return __( 'Exemple Modules', 'elementor-modules' );
	}

	/**
	 * Get widget icon.
	 *
	 * Retrieve make-coffee widget icon.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget icon.
	 */
	public function get_icon():string {
		return 'fa fa-coffee';
	}
}
