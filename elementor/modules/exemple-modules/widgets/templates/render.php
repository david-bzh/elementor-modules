<?php
/**
 * Template render
 *
 * @var Module_Base $this
 *
 * @package Elementor_Modules
 * @since 1.0.0
 */

	$settings = $this->get_settings_for_display();
?>

<div class="<?php echo esc_attr( $this->get_html_wrapper_class() ?? '' ); ?>">

	<?php echo wp_oembed_get( esc_url( $settings['url'] ) ); // phpcs:ignore WordPress.Security.EscapeOutput ?>

</div>
